import os
import random

import torch
import torch.nn as nn
from tqdm import tqdm

import config
from dataset import batch_to_train_data, load_prepare_data, trim_rare_words
from dataset import MAX_LENGTH
from model import EncoderRNN, DecoderRNN
from utils import set_seed, set_cuda_device


def maskNLLLoss(inp, target, mask):
    n = mask.sum()
    ce = -torch.log(torch.gather(inp, 1, target.view(-1, 1)).squeeze(1))
    loss = ce.masked_select(mask).mean()
    return loss, n.item()


def train(input_variable, lengths, target_variable, mask, 
          max_target_len, encoder, decoder, embedding,
          encoder_optimizer, decoder_optimizer, batch_size, 
          clip, teacher_forcing_ratio, max_length, device):

    encoder_optimizer.zero_grad()
    decoder_optimizer.zero_grad()

    input_variable = input_variable.to(device)
    target_variable = target_variable.to(device)
    mask = mask.to(device)
    lengths = lengths.to("cpu")

    loss = 0
    print_losses = []
    n_totals = 0

    encoder_outputs, encoder_hidden = encoder(input_variable, lengths)
    decoder_input = torch.LongTensor([[1 for _ in range(batch_size)]])
    decoder_input = decoder_input.to(device)
    decoder_hidden = encoder_hidden[:decoder.n_layers]

    use_teacher_forcing = True if random.random() < teacher_forcing_ratio else False

    if use_teacher_forcing:
        for t in range(max_target_len):
            decoder_output, decoder_hidden = decoder(
                decoder_input, decoder_hidden, encoder_outputs
            )
            decoder_input = target_variable[t].view(1, -1)
            mask_loss, nTotal = maskNLLLoss(decoder_output, target_variable[t], mask[t])
            mask_loss = mask_loss.to(device)
            loss += mask_loss
            print_losses.append(mask_loss.item() * nTotal)
            n_totals += nTotal
    else:
        for t in range(max_target_len):
            decoder_output, decoder_hidden = decoder(
                decoder_input, decoder_hidden, encoder_outputs
            )
            _, topi = decoder_output.topk(1)
            decoder_input = torch.LongTensor([[topi[i][0] for i in range(batch_size)]])
            decoder_input = decoder_input.to(device)
            mask_loss, nTotal = maskNLLLoss(decoder_output, target_variable[t], mask[t])
            loss += mask_loss
            print_losses.append(mask_loss.item() * nTotal)
            n_totals += nTotal

    loss.backward()

    _ = nn.utils.clip_grad_norm_(encoder.parameters(), clip)
    _ = nn.utils.clip_grad_norm_(decoder.parameters(), clip)

    encoder_optimizer.step()
    decoder_optimizer.step()

    return sum(print_losses) / n_totals


def train_iters(model_name, voc, pairs, encoder, decoder, 
                encoder_optimizer, decoder_optimizer, embedding, 
                encoder_n_layers, decoder_n_layers, save_dir, 
                n_iteration, batch_size, print_every, save_every, 
                clip, teacher_forcing_ratio, out_filename, device):

    start_iteration = 1
    print_loss = 0

    for iteration in tqdm(range(start_iteration, n_iteration + 1)):
        training_batch = batch_to_train_data(voc, [random.choice(pairs) for _ in range(batch_size)])
        input_variable, lengths, target_variable, mask, max_target_len = training_batch

        loss = train(input_variable, lengths, target_variable, mask, max_target_len, 
                     encoder, decoder, embedding, encoder_optimizer, decoder_optimizer, 
                     batch_size, clip, teacher_forcing_ratio, MAX_LENGTH, device)
        print_loss += loss

        if iteration % print_every == 0:
            print_loss_avg = print_loss / print_every
            with open(out_filename, 'a') as f:
                f.write(f'iter: {iteration}, loss: {print_loss_avg}\n')
            print_loss = 0

        if (iteration % save_every == 0):
            directory = os.path.join(save_dir, model_name)
            if not os.path.exists(directory):
                os.makedirs(directory)
            torch.save({
                'iteration': iteration,
                'en': encoder.state_dict(),
                'de': decoder.state_dict(),
                'en_opt': encoder_optimizer.state_dict(),
                'de_opt': decoder_optimizer.state_dict(),
                'loss': loss,
                'voc_dict': voc.__dict__,
                'embedding': embedding.state_dict()
            }, os.path.join(directory, '{}_{}.tar'.format(iteration, 'checkpoint')))


def main(config):
    voc, pairs = load_prepare_data(config.corpus_name, config.processed_data_path)
    pairs = trim_rare_words(voc, pairs, min_count=3)
    embedding = nn.Embedding(voc.num_words, config.hidden_size)
    encoder = EncoderRNN(config.hidden_size, embedding, 
                         config.encoder_n_layers, config.dropout).to(config.device)
    decoder = DecoderRNN(config.attn_model, embedding, config.hidden_size, voc.num_words, 
                         config.decoder_n_layers, config.dropout).to(config.device)
    
    encoder.train()
    decoder.train()

    encoder_optimizer = torch.optim.Adam(encoder.parameters(), lr=config.learning_rate)
    decoder_optimizer = torch.optim.Adam(decoder.parameters(), lr=config.learning_rate*config.decoder_learning_ratio)

    for state in encoder_optimizer.state.values():
        for k, v in state.items():
            if isinstance(v, torch.Tensor):
                state[k] = v.cuda()

    for state in decoder_optimizer.state.values():
        for k, v in state.items():
            if isinstance(v, torch.Tensor):
                state[k] = v.cuda()

    train_iters(config.model_name, voc, pairs, encoder, decoder, encoder_optimizer, 
                decoder_optimizer, embedding, config.encoder_n_layers, config.decoder_n_layers, 
                config.save_dir, config.n_iteration, config.batch_size, config.print_every, config.save_every, 
                config.clip, config.teacher_forcing_ratio, config.out_filename, config.device)


if __name__ == '__main__':
    set_seed(config.seed)
    set_cuda_device(config.gpu)
    main(config)
    