from collections import OrderedDict
import os

import torch
import torch.nn as nn

import config
from dataset import evaluate, normalize_string, Vocab
from model import EncoderRNN, DecoderRNN
from utils import set_seed, set_cuda_device


def evaluate_input(encoder, decoder, searcher, voc, device):
    input_sentence = ''
    while(1):
        try:
            input_sentence = input('> ')
            if input_sentence == 'q' or input_sentence == 'quit': break
            input_sentence = normalize_string(input_sentence)
            output_words = evaluate(encoder, decoder, searcher, voc, input_sentence, device)
            output_words = list(OrderedDict.fromkeys(output_words))
            output_words[:] = [x for x in output_words if not (x == 'EOS' or x == 'PAD')]
            print('', ' '.join(output_words))

        except KeyError:
            print("Error: Encountered unknown word.")


class GreedySearchDecoder(nn.Module):
    def __init__(self, encoder, decoder, device):
        super(GreedySearchDecoder, self).__init__()
        self.encoder = encoder
        self.decoder = decoder
        self.device = device

    def forward(self, input_seq, input_length, max_length):
        encoder_outputs, encoder_hidden = self.encoder(input_seq, input_length)
        decoder_hidden = encoder_hidden[:self.decoder.n_layers]
        decoder_input = torch.ones(1, 1, device=self.device, dtype=torch.long) * 1
        all_tokens = torch.zeros([0], device=self.device, dtype=torch.long)
        all_scores = torch.zeros([0], device=self.device)
        for _ in range(max_length):
            decoder_output, decoder_hidden = self.decoder(decoder_input, decoder_hidden, encoder_outputs)
            decoder_scores, decoder_input = torch.max(decoder_output, dim=1)
            all_tokens = torch.cat((all_tokens, decoder_input), dim=0)
            all_scores = torch.cat((all_scores, decoder_scores), dim=0)
            decoder_input = torch.unsqueeze(decoder_input, 0)
        return all_tokens, all_scores


def predict(config):
    load_filename = os.path.join(config.save_dir, config.model_name, 
                                 '{}_checkpoint.tar'.format(config.checkpoint_iter))
    checkpoint = torch.load(load_filename)
    voc = Vocab()
    voc.__dict__ = checkpoint['voc_dict']
    
    embedding = nn.Embedding(voc.num_words, config.hidden_size)
    embedding.load_state_dict(checkpoint['embedding'])
    encoder = EncoderRNN(config.hidden_size, embedding, 
                         config.encoder_n_layers, config.dropout)
    decoder = DecoderRNN(config.attn_model, embedding, config.hidden_size, 
                         voc.num_words, config.decoder_n_layers, config.dropout)
    encoder.load_state_dict(checkpoint['en'])
    decoder.load_state_dict(checkpoint['de'])
    encoder = encoder.to(config.device)
    decoder = decoder.to(config.device)
    
    encoder.eval()
    decoder.eval()

    searcher = GreedySearchDecoder(encoder, decoder, config.device)
    evaluate_input(encoder, decoder, searcher, voc, config.device)

if __name__ == '__main__':
    set_seed(config.seed)
    set_cuda_device(config.gpu)
    predict(config)
