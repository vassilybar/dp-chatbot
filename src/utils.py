import os
import random
import torch
import numpy as np


def set_seed(seed):
    random.seed(seed)
    np.random.seed(seed)
    torch.manual_seed(seed)
    torch.cuda.manual_seed(seed)
    torch.cuda.manual_seed_all(seed)
    os.environ['PYTHONHASHSEED'] = str(seed)


def set_cuda_device(cuda_num):
    os.environ['CUDA_VISIBLE_DEVICES'] = str(cuda_num)
    os.environ['CUDA_DEVICE_ORDER'] = 'PCI_BUS_ID'
    torch.backends.cudnn.deterministic = True
