raw_data_path = '../Ubuntu-dialogue-corpus/dialogueText.csv'
processed_data_path = '../dialogue_pairs.txt'
out_filename = '../logfile4.txt'

seed = 60
gpu = 0
device = 'cuda'

model_name = 'cb_model'
attn_model = 'dot'
hidden_size = 500
encoder_n_layers = 2
decoder_n_layers = 2
dropout = 0.1
batch_size = 256

save_dir = 'weights'
corpus_name = 'ubuntu_qa'

clip = 50.0
teacher_forcing_ratio = 0.5
learning_rate = 0.0001
decoder_learning_ratio = 5.0
n_iteration = 200000
print_every = 200
save_every = 20000

checkpoint_iter = 200000