import csv

import pandas as pd
from tqdm import tqdm

import config


def extractSentencePairs(df):
    qa_pairs = []
    for i in tqdm(range(len(df) - 1)):
        if df.iloc[i].dialogueID != df.iloc[i+1].dialogueID:
            continue
        line = df.iloc[i]['text'].strip()
        line_next = df.iloc[i+1]['text'].strip()
        if line and line_next:
            qa_pairs.append([line, line_next])
    return qa_pairs


if __name__ == '__main__':
    df = pd.read_csv(config.raw_data_path)
    df = df.sort_values(['dialogueID', 'date'])
    df = df.fillna('')
    
    qa_pairs = extractSentencePairs(df)
    
    with open(config.processed_data_path, 'w', encoding='utf-8') as f:
        out = csv.writer(f, delimiter='\t', lineterminator='\n')
        for pair in tqdm(qa_pairs):
            _ = out.writerow(pair)
