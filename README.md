# NSU DP Coursework
- This is my code for Neural-network-based chat bot
- Training data can be found [here](https://www.kaggle.com/rtatman/ubuntu-dialogue-corpus)
## How to run
### Preprocessing
~~~
$ python src/preprocess.py
~~~
### Training 
~~~
$ python src/train.py
~~~
[train.py](src/train.py) trains seq2seq model with parameters from [config.py](src/config.py)
### Predicting 
~~~
$ python src/predict.py
~~~
[predict.py](src/predict.py) makes predictions for user inputs in command line